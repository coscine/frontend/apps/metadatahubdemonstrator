# Metadata Hub Demonstrator

This application is a prototype demonstration of the Metadata Hub.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn dev
```

### Compiles and minifies for production
```
yarn build
```

### Runs the minified production build
```
yarn preview
```

### Lints files
```
yarn lint
```

### Lints and fixes files
```
yarn lint:fix
```
