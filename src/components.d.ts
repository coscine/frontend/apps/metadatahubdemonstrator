// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    BButton: typeof import('bootstrap-vue')['BButton']
    BContainer: typeof import('bootstrap-vue')['BContainer']
    BFormFile: typeof import('bootstrap-vue')['BFormFile']
    BFormGroup: typeof import('bootstrap-vue')['BFormGroup']
    BFormInput: typeof import('bootstrap-vue')['BFormInput']
    BFormSelect: typeof import('bootstrap-vue')['BFormSelect']
    BFormSelectOption: typeof import('bootstrap-vue')['BFormSelectOption']
    BFormTextarea: typeof import('bootstrap-vue')['BFormTextarea']
  }
}
